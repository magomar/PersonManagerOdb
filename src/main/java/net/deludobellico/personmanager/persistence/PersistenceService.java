package net.deludobellico.personmanager.persistence;

import net.deludobellico.personmanager.model.Person;

import javax.persistence.*;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *  Created on 26/07/2016.
 */
public class PersistenceService {
    private EntityTransaction et;
    private EntityManager em;

    public PersistenceService(String persistenceUnit) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnit);
        em = emf.createEntityManager();
        et = em.getTransaction();
    }

    public List<Person> getAllPersons() throws DaoException {
        List<Person> persons = execute(Person.findAll, Person.class);
//        em.clear();
        return persons;
    }

    public Person getPersonById(int id) throws DaoException {
        Person person = execute(Person.findById, Person.class, "id", id);
        if (null != person) detach(person);
        return person;
    }

    public void addPerson(Person person) throws DaoException {
        try {
            et.begin();
            em.persist(person);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    public void removePerson(Person person) throws DaoException {
        try {
            et.begin();
            em.remove(person);
            et.commit();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    public Person savePerson(Person person) throws DaoException {
        try {
            et.begin();
            Person merged = em.merge(person);
            et.commit();
            return merged;
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }

    public void save() {
        et.begin();
        em.flush();
        et.commit();
    }

    public void detach(Object object) {
        em.detach(object);
    }


    public void clear() {
        em.clear();
    }

    public void close() {
        et.begin();
        et.commit();
        em.close();
    }

    private <T> List<T> execute(String queryName, Class<T> type) throws DaoException {
        try {
            TypedQuery<T> query = em.createNamedQuery(queryName, type);
            return query.getResultList();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }
    private <T> T execute(String queryName, Class<T> type, String paramName, Object paramValue) throws DaoException {
        try {
            TypedQuery<T> query = em.createNamedQuery(queryName, type).setParameter(paramName, paramValue);
            return query.getSingleResult();
        } catch (Exception e) {
            throw new DaoException(e);
        } finally {
            if (et.isActive())
                et.rollback();
        }
    }
}
