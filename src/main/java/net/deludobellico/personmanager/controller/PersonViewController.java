package net.deludobellico.personmanager.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.deludobellico.personmanager.model.Person;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author mario
 */
public class PersonViewController implements Initializable {

    @FXML
    private TextField firstNameText;
    @FXML
    private TextField lastNameText;
    @FXML
    private Button buttonOK;
    private Optional<Person> person;
    protected Stage stage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stage = new Stage();
        final BooleanBinding invalidName = Bindings.isEmpty(firstNameText.textProperty())
                .or(Bindings.isEmpty(lastNameText.textProperty()));
        buttonOK.disableProperty().bind(invalidName);
        person = null;
    }

    public static PersonViewController init(Stage owner, Modality modality) {
        FXMLLoader fxmlLoader = new FXMLLoader(PersonViewController.class.getResource("/net/deludobellico/personmanager/view/PersonView.fxml"));
        PersonViewController controller = null;
        try {
            Parent parent = fxmlLoader.load();
            controller = fxmlLoader.getController();
            controller.stage.setScene(new Scene(parent));
            controller.stage.initOwner(owner);
            controller.stage.initModality(modality);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

    public Optional<Person> showAndWait(Person person) {
        this.person = Optional.of(person);
        firstNameText.textProperty().bindBidirectional(person.firstNameProperty());
        lastNameText.textProperty().bindBidirectional(person.lastNameProperty());
        stage.showAndWait();
        return this.person;
    }

    public Optional<Person> showAndWait() {
        person = Optional.empty();
        stage.showAndWait();
        return person;
    }

    @FXML
    private void onOK(ActionEvent event) {
        if (!person.isPresent()) {
            Person newPerson = new Person(firstNameText.getText(), lastNameText.getText());
            person = Optional.of(newPerson);
        }
        stage.close();
    }

    @FXML
    private void onCancel(ActionEvent event) {
        person = Optional.empty();
        stage.close();
    }

}
