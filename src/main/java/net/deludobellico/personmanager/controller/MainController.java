package net.deludobellico.personmanager.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.deludobellico.personmanager.model.Person;
import net.deludobellico.personmanager.persistence.DaoException;
import net.deludobellico.personmanager.persistence.PersistenceService;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 26/07/2016.
 */
public class MainController implements Initializable {
    @FXML
    private ListView<Person> listView;
    @FXML
    private Button buttonAdd;
    @FXML
    private Button buttonRemove;
    @FXML
    private Button buttonModify;

    private ObservableList<Person> data = null;
    private PersistenceService persistence;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        persistence = new PersistenceService("PersonManager");
        List<Person> savedPersons = null;
        try {
            savedPersons = persistence.getAllPersons();
            data = FXCollections.observableArrayList(savedPersons);
            listView.setItems(data);
        } catch (DaoException e) {
            e.printStackTrace();
        }

        final BooleanBinding noPersonSelected = Bindings.isNull(
                listView.getSelectionModel().selectedItemProperty());

        buttonRemove.disableProperty().bind(noPersonSelected);
        buttonModify.disableProperty().bind(noPersonSelected);
    }

    @FXML
    private void remove(ActionEvent event) throws DaoException {
        Person selectedPerson = listView.getSelectionModel().getSelectedItem();
        data.remove(selectedPerson);
        persistence.removePerson(selectedPerson);
    }

    @FXML
    private void add(ActionEvent event) throws DaoException {
        final Stage stage = (Stage) listView.getScene().getWindow();
        PersonViewController pvc = PersonViewController.init(stage, Modality.APPLICATION_MODAL);
        Optional<Person> newGuy = pvc.showAndWait();
        if (newGuy.isPresent()) {
            data.add(newGuy.get());
            persistence.addPerson(newGuy.get());
        }
    }

    @FXML
    private void modify(ActionEvent event) throws DaoException {
        final Stage stage = (Stage) listView.getScene().getWindow();
        PersonViewController pvc = PersonViewController.init(stage, Modality.APPLICATION_MODAL);
        Person selectedPerson = listView.getSelectionModel().getSelectedItem();
        persistence.detach(selectedPerson);
        if (pvc.showAndWait(selectedPerson).isPresent()) {
            persistence.savePerson(selectedPerson);
            listView.refresh();
        }
    }

}