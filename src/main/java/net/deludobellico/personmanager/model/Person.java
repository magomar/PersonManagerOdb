package net.deludobellico.personmanager.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.*;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 26/07/2016.
 */
@Entity
@Access(AccessType.PROPERTY)
@NamedQueries({
        @NamedQuery(name = Person.findAll, query = "SELECT p FROM Person p"),
        @NamedQuery(name = Person.findById, query = "SELECT p FROM Person p WHERE p.id = :id")
})
public class Person {
    public final static String PREFIX = "Person";
    public final static String findAll = PREFIX + ".findAll";
    public final static String findById = PREFIX + ".findById";
    @Id
    @GeneratedValue
    private int id;
    private final StringProperty firstName = new SimpleStringProperty();
    private final StringProperty lastName = new SimpleStringProperty();

    public Person(String firstName, String lastName) {
        this.firstName.set(firstName);
        this.lastName.set(lastName);
    }

    public Person() {

    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    @Override
    public String toString() {
        return firstName.get() + " " + lastName.get();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        return lastName != null ? lastName.equals(person.lastName) : person.lastName == null;

    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        return result;
    }
}
